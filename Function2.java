package Functionaltask;
import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
class Regexx{
	static Scanner scanner = new Scanner(System.in);
	static File fil = new File("D:\\logs");
	static File f[] = fil.listFiles();
	static ArrayList<Future<?>> futures = new ArrayList<Future<?>>();
	
	static ExecutorService th = Executors.newFixedThreadPool(5);
	
	public static void readfile() throws IOException {
		@SuppressWarnings("resource")
		FileWriter fileWriter=new FileWriter("D:\\temp\\temp.txt");
		for (File file : f){
			Runnable reg5 = new Runnable() {
				public void run() {
					Scanner in;
					try {
				        in = new Scanner(file);
						while (in.hasNextLine()){
							String line = in.nextLine();
							Pattern pm = Pattern.compile(":.*?Exception");
							Matcher mp = pm.matcher(line);
							if (mp.find()){
								fileWriter.write(line+"\n");
								while (in.hasNextLine()){
									String l = in.nextLine();
									if (Pattern.compile("at|Caused").matcher(l).find())
										fileWriter.write(l+"\n");									
									else
										break;
								}
							}
						}
			         }
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			Future<?> fur = th.submit(reg5);
			futures.add(fur);
		}
	}
	
	static void keywordtofile(){
		System.out.println("Enter the keyword to be fetched");
		String date = scanner.nextLine();
		for (File file : f){
			Runnable reg1 = new Runnable(){
				public void run(){
					try{
						Scanner in = new Scanner(file);
						System.out.println("\n\n\nThis date is present on " + file.getName() + "\n\n\n");
						while (in.hasNextLine()) {
							String line = in.nextLine();
							if (Pattern.compile(date).matcher(line).find()) {
								System.out.println(line + " " + Thread.currentThread().getName());
							}
						}
					} catch (FileNotFoundException e){
						e.printStackTrace();
					}
				}
			};
			Future<?> fur = th.submit(reg1);
			futures.add(fur);
		}
	}
		
	static void datetofilecontent() throws IOException {
		System.out.println("Enter the Date to read the file content :-   Enter in the format xx-xx-xxx");
		String dates = scanner.nextLine();
		if (Pattern.matches("^\\d{2}-\\d{2}-\\d{4}$", dates)){
			for (File file : f) {
				Runnable reg3 = new Runnable(){
					public void run(){
						Scanner in;
						try {
							in = new Scanner(file);
							while (in.hasNextLine()) {
								String line = in.nextLine();
								Pattern pm = Pattern.compile(dates);
								Matcher mp = pm.matcher(line);
								if (mp.find()) {
									System.out.println(line);
									while (in.hasNextLine()) {
										String l = in.nextLine();
										Pattern p = Pattern.compile("\\d{2}-\\d{2}-\\d{4}");
										Matcher m = p.matcher(l);
										if (m.find())
											break;
										else {
											System.out.println(l);
										}
										
										
									}
								}
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (@SuppressWarnings("hiding") IOException e) {
							e.printStackTrace();
						}
					}
				};
				Future<?> fur = th.submit(reg3);
				futures.add(fur);
			}
		} else
			System.out.println("The Date is not in Proper Format");
	}

	static void datetoexception() {

		System.out.println("Enter the Date to fetch the exceptions occurs :-   Enter in the format xx-xx-xxx\n");
		String dat = scanner.nextLine();
		if (Pattern.matches("^\\d{2}-\\d{2}-\\d{4}$", dat)) {
				File file = new File("D:\\temp\\temp.txt");
				Runnable reg2 = new Runnable() {
					public void run(){
						try {
							@SuppressWarnings("resource")
							Scanner in = new Scanner(file);
							while (in.hasNextLine()) {
								String line = in.nextLine();
								Pattern pm = Pattern.compile(dat);
								Matcher mp = pm.matcher(line);
								if (mp.find()){
									while (true){
										if (in.hasNextLine()){
											String l = in.nextLine();
											Pattern p = Pattern.compile("\\d{2}-\\d{2}-\\d{4}");
											Matcher m = p.matcher(l);
											if (m.find())
												break;
											else {
												Pattern pat = Pattern.compile(":.*?Exception");
												Matcher mat = pat.matcher(l);
												if (mat.find())
													System.out.println("Exception " + mat.group());
											}
										}
									}
								}
							}

						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
					}
				};
				Future<?> fur = th.submit(reg2);
				futures.add(fur);		
		} else {
			System.out.println("The Date is not in Proper Format");
		}
	}
	
	static void exceptiontostacktrace() throws IOException {
		exceptions();
		System.out.println("Enter the Exception :- Enter in the format of xxxx.xxxx.xxxxException");
		String ex1 = scanner.nextLine();
		if (Pattern.matches(".*?Exception", ex1)){
			File file =new File("D:\\temp\\temp.txt");
			Runnable reg5 = new Runnable() {
				public void run() {
					Scanner in;
					try {
				        in = new Scanner(file);
						while (in.hasNextLine()) {
							System.out.println(in.nextLine());
						}
			         }
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			Future<?> fur = th.submit(reg5);
			futures.add(fur);
		} 
		else {
			System.out.println("Your Exception is not in Proper format");
		}
	}

	static void exceptiontodate() throws FileNotFoundException {
		exceptions();
		System.out.println("Enter the Exception :- Enter in the format of xxxx.xxxx.xxxxException");
		String ex1 = scanner.nextLine();
		if (Pattern.matches(".*?Exception", ex1)){
			File file =new File("D:\\temp\\temp.txt");
			Runnable reg4 = new Runnable() {
				public void run() {
					Scanner in;
					try {
				        in = new Scanner(file);
						while (in.hasNextLine()) {
							String line = in.nextLine();
							Pattern pm = Pattern.compile("\\d{2}-\\d{2}-\\d{4}");
							Matcher mp = pm.matcher(line);
							if (mp.find()){
								while (in.hasNextLine()) {
									String l = in.nextLine();
									Pattern p = Pattern.compile(ex1);
									Matcher m = p.matcher(l);
									if (m.find())
										System.out.println(mp.group());
									else
										break;
								}
							}
						}
			         }
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			Future<?> fur = th.submit(reg4);
			futures.add(fur);
		} 
		else {
			System.out.println("Your Exception is not in Proper format");
		}	
	}
	
	static void exceptions() {

		System.out.println("1.java.lang.Exception\n2.java.sql.SQLException\n3.com.zoho.cp.NoPermitsAvailableException\n4.java.io.IOException\n5.java.lang.NullPointerException\n6.javax.servlet.ServletException");
		
	}

}

public class Function2 {
	@SuppressWarnings("resource")
	public static void main(String[] args) throws ExecutionException, InterruptedException, IOException{
		Scanner scanner = new Scanner(System.in);
	    new PrintWriter(new FileWriter("D:\\temp\\temp.txt")).flush();
		String n="-1";
		System.out.println("File is reading.........");
		Regex.readfile();
		for (Future<?> fu : Regexx.futures)
			fu.get();
		while (!n.equals("6")){
			System.out.println("\n\n1.keyword to full line\n2.date to file content\n3.Date to exception\n4.exception to stackTrace\n5.exception to date\n6.End");
			System.out.println("\nEnter task to be performed\n");
			n=scanner.nextLine();
			switch(n){
				case "1":
					Regexx.keywordtofile();  				
					break;
				case "2":
					Regexx.datetofilecontent();
					break;
				case "3":
					Regexx.datetoexception();
					break;
				case "4":
					Regexx.exceptiontostacktrace();
					break;
				case "5":
					Regexx.exceptiontodate();
					break;
				case "6":
					System.out.println("Your program Has been Ended");
				    new PrintWriter(new FileWriter("D:\\temp\\temp.txt")).flush();
					System.out.println("-----------Cache Cleared---------");
					break;
				default:
					System.out.println("Enter a valid input");
				}
			
			for (Future<?> fu : Regexx.futures)
				fu.get();
			} 
			
			
		}
}
